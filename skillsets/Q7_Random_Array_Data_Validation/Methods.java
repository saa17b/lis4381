import java.util.Scanner;
import java.util.Random;

public class Methods

{

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Sebastian Arcila");
        System.out.println("Program promps user to enter desired number of pseudorandom-generated integers (min 1). \n");
        System.out.println("Program validates user input for integers greater than 0.");
        
        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println();
    }

    public static int[] createArray()
    {
        Scanner src = new Scanner(System.in);
        int arraySize = 0;
            
            System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
            
            while (!src.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                src.next();
                System.out.print("Please try again. Enter valid integer (min 1): ");
            }
            arraySize = src.nextInt();

            while(arraySize < 1)
            {
                System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");
                while(!src.hasNextInt())
                {
                    System.out.print("\nNumber must be an integer: ");
                    src.next();
                    System.out.print("Please try again. Enter integer value greater than 0: ");
                }
                arraySize = src.nextInt();
            }

        int myArray[] = new int[arraySize];
        return myArray;
    
    
    }

    public static void generatePseudoRandomNumbers(int[] myArray)
    {
        Random r = new Random();
        int i = 0;
        System.out.println("For loop: ");
        for(i=0;i<myArray.length;i++)
        {
            System.out.println(r.nextInt());
        }

        System.out.println("\nEnhanced for loop: ");
        for(int n: myArray)
        {
            System.out.println(r.nextInt());
        }

        System.out.println("\nWhile loop: ");
        i=0;
        while (i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }

        i=0;
        System.out.println("\ndo...while loop: ");
        do
        {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
    }

}