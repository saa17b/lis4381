import java.util.Scanner;

public class Methods

{
    public static void largestNumber()
    {
        Scanner src = new Scanner(System.in);
            
            System.out.print("Enter first integer: ");
            int num1 = src.nextInt();
            
            System.out.print("Enter second integer: ");
            int num2 = src.nextInt();

            if(num1 > num2)
            {
                System.out.println("\n" + num1 + " is larger than " + num2);
            }
            else if(num2 > num1)
            {
                System.out.println("\n" + num2 + " is larger than " + num1);
            }
            else{
                System.out.println("\nIntegers are equal.");
            }
    }
}