import java.util.Scanner;


public class Main 
{
    public static void main(String args[])
    {
        System.out.println("\nDeveloper: Sebastian Arcila\n");
        System.out.println("Program evaluates the largest of two integers.\n");

        Methods.largestNumber();
    }
}
