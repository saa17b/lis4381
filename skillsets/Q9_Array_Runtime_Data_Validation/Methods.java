import java.util.Scanner;
import java.util.Random;

public class Methods

{

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Sebastian Arcila\n");
        System.out.println("1) Program creates array size at run-time");
        System.out.println("2) Program displays array size");
        System.out.println("3) Program rounds sum and average of numbers to two decimal places.");
        System.out.println("4) Numbers *must* be float data type, not double.");

        System.out.println();
    }

    public static int validateArraySize()
    {
        Scanner src = new Scanner(System.in);
        int arraySize = 0;
            
            System.out.print("Please enter array size: ");
            while (!src.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                src.next();
                System.out.print("Please try again. Enter array size: ");
            }
            arraySize = src.nextInt();
            System.out.println();

        return arraySize;
    }

    public static void calculateNumbers(int arraySize)
    {
        Scanner src = new Scanner(System.in);

        float sum = 0.0f;
        float average = 0.0F;

        System.out.print("Plese enter " + arraySize + " numbers.\n");

        float numsArray[] = new float[arraySize];

        for (int i=0; i<arraySize;i++)
        {
            System.out.print("Enter num " + (i + 1) + ": ");

            while(!src.hasNextFloat())
            {
                System.out.println("Not valid number!\n");
                src.next();
                System.out.print("Please try again. Enter num " + (i + 1) + ": ");
            }
            numsArray[i] = src.nextFloat();
            sum = sum + numsArray[i];
        }
        average = sum / arraySize;

        System.out.print("\nNumbers entered: ");
        for (int i=0;i<numsArray.length;i++)
        {
            System.out.print(numsArray[i] + " ");
        }

        System.out.println("\nSum: " + sum);
        System.out.println("Average: " + average);

    }

}