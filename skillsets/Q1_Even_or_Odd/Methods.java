import java.util.Scanner;

public class Methods

{
    public static void evenOrOdd()
    {
        Scanner src = new Scanner(System.in);
            
        System.out.print("Enter integer: ");
        int num = src.nextInt();

        if (num % 2 == 0)
        {
            System.out.println(num + " is an even integer.");
        }
        else
        {
            System.out.println(num + " is an odd integer.\n");
        }
            
    }
}