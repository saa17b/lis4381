import java.util.Scanner;


public class Main 
{
    public static void main(String args[])
    {
        System.out.println("\nDeveloper: Sebastian Arcila\n");
        System.out.println("Program evaluates integers as even or odd.\n");
        
        Methods.evenOrOdd();

    }
}
