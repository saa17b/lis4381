import java.util.Scanner;

public class Methods

{

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Sebastian Arcila");
        System.out.println("Rain Detector");
        System.out.println("\nProgram Requirements:");
        System.out.println("1. Capture user-entered input.");
        System.out.println("2. Based upon day of week, determine if rain will occur.");
        System.out.println("3. Note: Program does case-insensitive comparison.\n");
    }

    public static void rainCheck()
    {
        Scanner src = new Scanner(System.in);
        String[] weekdayArr = new String[]{"monday", "tuesday", "wednesday", "thursday", "friday"};
            
            System.out.println("Input: ");
            System.out.print("Enter full name of day of week: ");
            String day = src.nextLine().toLowerCase();
            
            for(int i=0;i<weekdayArr.length;i++)
            {
                if(day.equals(weekdayArr[i]))
                {
                    System.out.println("\nOutput:");
                    System.out.println("Better get an umbrella!");
                    break;
                }
                else
                {
                    System.out.println("\nOutput:");
                    System.out.println("Whoopee! It's the weekend!!!");
                    break;
                }
            }
    }
}