> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Android App
2. Complete skill sets 1-3 implementing OOP programming in java
3. Compile and run the java applications

#### README.md file should include the following items:

* Screenshots of the first and secod user interface for the Healthy Recipes app
* Screenshots of the java apps running

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface Running](img/ui1.png)

*Screenshot of second user interface running*:

![Second User Interface Running](img/ui2.png)

*Screenshot of skill sets 1*:

![SS1: Even or Odd](img/ss1.png)

*Screenshot of skill sets 2*:

![SS2: Largest of Two Integers](img/ss2.png)

*Screenshot of skill sets 3*:

![SS3: Arrays and Loops](img/ss3.png)

