> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Project 2 Requirements:

*Three Parts:*

1. Add edit functionality to A5
2. Add delete functionality to A5
3. Add RSS Feed to website


#### README.md file should include the following items:

* Screenshot of the website's home page
* Screenshot of P2 page
* Screenshot of failed attempt to edit due to incorrect use of characters (client and server side)
* Screenshot of before and after successful edit
* Screenshots of delete prompt and successfully deleted record
* Screenshots of RSS Feed
* Link to local lis4381 web app 
    * [http://localhost:8080/lis4381/](http://localhost:8080/lis4381/)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Project Screenshots:

*Screenshot of website's home page*:

![Home page](img/home.png)

*Screenshot of P2 page*:

![P2 page](img/p2-home.png)

*Screenshot of failed attempt to edit on the client-side*:

![Failed Validation](img/failed-validation.png)

*Screenshot of failed validation on the server-side*:

![Failed Validation](img/failed-validation-error.png)

*Screenshot of record before being edited*:

![Before edit](img/p2-home.png)

*Screenshot of record after being edited*:

![Successful edit](img/passed-validation.png)

*Screenshot of RSS Feed*:

![RSS Feed](img/rss-feed.png)



