> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Assignment 3 Requirements:

*Three Parts:*

1. Create My Event Android App
2. Complete skill sets 4-6 implementing OOP programming in java
3. Compile and run the java applications
4. Create a database solution for a pet store owner

#### README.md file should include the following items:

* Screenshots of the first and secod user interface for the My Event app
* Screenshots of the java apps running
* Screenshots of ERD
*Screenshots of 10 record for each table
* Links to [a3.mwb](docs/a3.mwb) and [a3.sql](docs/a3.sql) 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface Running](img/ui1.png)

*Screenshot of second user interface running*:

![Second User Interface Running](img/ui2.png)

*Screenshot of skill sets 4*:

![SS1: Even or Odd](img/ss4.png)

*Screenshot of skill sets 5*:

![SS2: Largest of Two Integers](img/ss5.png)

*Screenshot of skill sets 6*:

![SS3: Arrays and Loops](img/ss6.png)

