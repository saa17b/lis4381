> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Assignment 4 Requirements:

*Three Parts:*

1. Create a website to showcase our work
2. Complete skill sets 10-12 implementing OOP programming in java
3. Add client-side data validation

#### README.md file should include the following items:

* Screenshots of home page
* Screenshots of failed validation
* Screenshots of skillsets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of home page*:

![Home page](img/homepage.png)

*Screenshot of failed validation*:

![Failed Validation](img/failed-validation.png)

*Screenshot of passed validation*:

![Passed Validation](img/passed-validation.png)

*Screenshot of skill sets 10*:

![SS10: Random Array Data Validation](img/ss10.png)

*Screenshot of skill sets 11*:

![SS11: Largest Three Numbers](img/ss11.png)

*Screenshot of skill sets 12*:

![SS12: Array Runtime Validation](img/ss12.png)


