> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Sebastian Arcila

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    - set up version control
    - installed ampps
    - installed java and created my first program
    - installed android studio and create my first app
2. [A2 README.MD](a2/README.md)
    - created android app with two UI's
    - completed skill sets 1-3
3. [A3 README.MD](a3/README.md)
    - Create My Event Android App
    - Complete skill sets 4-6 implementing OOP programming in java
    - Compile and run the java applications
    - Create a database solution for a pet store owner
4. [P1 README.MD](p1/README.md)
    - Create Business Card Android app
    - Complete skillsets 7-9 implementing OOP programming in java
    - Add launcher icon to application
5. [A4 README.MD](a4/README.md)
    - Create a website to showcase our work
    - Complete skillsets 10-12 implementing OOP programming in java
    - Add client-side data validation
6. [A5 README.MD](a5/README.md)
    - Connect the database that we created in A3 to our website running locally
    - Display our database data on website
    - Add server side validation to ensure the data being added is accurate
    - Allow users to add data to our database and display changes made in real time
    - Complete skill sets 13-15 using java and php
7. [P2 README.MD](p2/README.md)
    - Add edit functionality to A5
    - Add delete functionality to A5
    - Add RSS Feed to website