> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Assignment 5 Requirements:

*Five Parts:*

1. Connect the database that we created in A3 to our website running locally
2. Display our database data on website
3. Add server side validation to ensure the data being added is accurate
4. Allow users to add data to our database and display changes made in real time
5. Complete skill sets 13-15 using java and php


#### README.md file should include the following items:

* Screenshots of Assignment 5 home page
* Screenshots of invalid input on Add Pet Store page (client-side)
* Screenshots of invalid input on Add Pet Store page (server-side)
* Screenshots of the information of the Pet Store that is being added
* Screenshots proving that the Pet Store which was just added is in the database
* Screenshots of skillsets 13-15
* Link to local lis4381 web app 
    * [http://localhost:8080/lis4381/](http://localhost:8080/lis4381/)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of home page*:

![Home page](img/index.png)

*Screenshot of failed validation on the client-side*:

![Failed Validation](img/add_petstore_invalid-1.png)

*Screenshot of failed validation on the server-side*:

![Failed Validation](img/add_petstore_process_invalid-2.png)

*Screenshot of Pet Store information being added to the database*:

![Passed Validation](img/add_petstore_valid-1.png)

*Screenshot proving that Pet Store was succesfully added to the database*:

![Passed Validation](img/add_petstore_process_valid-2.png)

*Screenshot of skill sets 13 (java)*:

![SS13: Sphere Volume](img/ss13.png)

*Screenshot of skill sets 14 (php)*:

![SS14: Simple Calculator](img/ss14-1.png)
![SS14: Simple Calculator](img/ss14-2.png)
![SS14: Simple Calculator](img/ss14-3.png)
![SS14: Simple Calculator](img/ss14-4.png)

*Screenshot of skill sets 15 (php)*:

![SS15: Write Read File](img/ss15-1.png)
![SS15: Write Read File](img/ss15-2.png)


