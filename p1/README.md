> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sebastian Arcila - saa17b

### Assignment 4 Requirements:

*Three Parts:*

1. Create Business Card Android App
2. Complete skill sets 7-9 implementing OOP programming in java
3. Add launcher icon to application

#### README.md file should include the following items:

* Screenshots of the first and secod user interface for the Business app
* Screenshots of the java apps running
* Screenshots of extra credit skillset

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface Running](img/ui1.png)

*Screenshot of second user interface running*:

![Second User Interface Running](img/ui2.png)

*Screenshot of skill sets 7*:

![SS7: Random Array Data Validation](img/ss7.png)

*Screenshot of skill sets 8*:

![SS8: Largest Three Numbers](img/ss8.png)

*Screenshot of skill sets 9*:

![SS9: Array Runtime Validation](img/ss9.png)

*Screenshot of skill sets extra credit*:

![Extra Credit: Rain Detector](img/ss.png)

